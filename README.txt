=== Plugin Name ===
Contributors: vivacityinfotech.jaipur
Tags:  twitter feeds, twitter timeline,twitter widget,twitter widget for wordpress,wp twitter widget,live tweets, live twitter feeds, tweets, feeds, twitter time line, twitter user tweets,  twitter 1.1, twitter api
Requires at least: 3.0
Tested up to: 3.8
License: GPLv2 or later
Stable tag: 1.2


WP Twitter Feeds - A simple widget which lets you add your latest tweets in just a few clicks on your website.

== Description ==

An easy Twitter feeds widget that provides facility to display twitter tweets on your website using the latest Twitter 1.1 API. A simple widget which lets you add your latest tweets in widget areas. Get started in just a few clicks and use the provided Widget to easily display your Tweets on your website.

Now you don`t need to copy and paste the code from twitter with your settings anymore, just install the plugin and set the options.
By option page, you can define the duration of fade effect, the number of tweet to see and other things.

The "WP Twitter Feeds" Widget will never require your Twitter password, because it based on Open Authentication (OAuth) keys and secrets, that you will get by creating your application at Twitter. It gets all your required data from API. Incase you change your password, you won’t need to update your Widget in your Blog.

= Plugin Features =

    *Easy install
    *Very easy to configure.
    *Display Tweets with a Widget.
    *You can choose to show a profile image, border color and much more with your Tweets.
    *Multiple instance so you can use Twitter widget multiple places.
    *Different color Options.
    *Lightweight and loading fast
    *Set maximum number of tweets to display
    *Turn on/off displaying avatar
    *Supports v1.1 of the Twitter API


= Rate Us / Feedback =

Please take the time to let us and others know about your experiences by leaving a review, so that we can improve the plugin for you and other users.

If you like the plugin please [Donate here](http://tinyurl.com/owxtkmt). 

= Want More? =

If You Want more functionality or some modifications, just drop us a line what you want and We will try to add or modify the plugin functions.



== Available Languages ==

    English

== Installation ==

Installation consists of following steps:

1. Upload "WP Twitter Tweets" to the /wp-content/plugins/ directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Place the "WP Twitter Tweets" Widget on your sidebar and add twitter username with API details of Twitter.

== Frequently Asked Questions ==

= How to create a Twitter Application =

1. Visit the Twitter Developers Site: The first thing you need to do is head on down to dev.twitter.com. In order to create an account, all you need to do is click on the “Sign In” link at the top right.

2. Sign in with your Twitter Account: Next, sign in with the Twitter account you want to associate with your app. You do have a Twitter account don’t you?!

3. Go to “My Applications”: Once you’re logged in, click on the downwards arrow to the right of your Twitter image and select “My Applications”. This is where all your registered Twitter apps will appear.

4. Create a New Application: If you are new to the Developers site you won’t see any applications registered. Either way, it’s time to create our first application. To do this, click on the big “Create a new application” button.

5. Fill in your Application Details: It’s been easy so far, but this screen often makes people confused. Don’t worry, it’s a lot easier than you think. First of all you need to give your app a unique name (one that no one else has used for their Twitter app). Since we’re going to be an creating app for personal use and not one that other people can register and use, just put your domain name in or perhaps even your name. You don’t have to worry much about the description– you can change this later. I’ve put “a set of Twitter tools for personal use”.Put your website in the website field– don’t worry that it isn’t (as Twitter ask) your application’s publicly accessible home page. However, this website will be where your app is hosted.For now ignore the Callback URL field. If you are allowing users to log into your app to authenticate themselves, you’d enter the URL where they would be returned after they’ve given permission to Twitter to use your app.Once you’ve done this, make sure you’ve read the “Developer Rules Of The Road” blurb, check the “Yes, I agree” box, fill in the CAPTCHA (don’t you just love them) and click the “create your Twitter Application” button. Hurrah!

6. Create Your Access Token.

7. Make a note of your OAuth Settings.

== Screenshots ==

1. WP Twitter Feeds plugin installed and appears in the plugins menu.
2. WP Twitter Feeds widget under Appearence->Widgets.
3. WP Twitter Feeds widget under Appearence->Widgets.
4. WP Twitter Feeds widget under Appearence->Widgets.
5. WP Twitter Feeds widget under Appearence->Widgets.

== Changelog ==

= 1.2 =
* Improved Fornt-End UI design.

= 1.1 =
* Boxed layout for tweets.
* Improved Twitter username validation in the widget.

= 1.0 =
* Initial release


